EESchema Schematic File Version 2
LIBS:subwoofer_LR-rescue
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:subwoofer_LR-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 3
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L CONN_01X02 P1
U 1 1 560AA734
P 700 1000
F 0 "P1" H 700 1150 50  0000 C CNN
F 1 "INPUT" V 800 1000 50  0000 C CNN
F 2 "Connect:bornier2" H 700 1000 60  0001 C CNN
F 3 "" H 700 1000 60  0000 C CNN
	1    700  1000
	-1   0    0    1   
$EndComp
$Comp
L TL072-RESCUE-subwoofer_LR U1
U 1 1 560AA952
P 2250 1050
F 0 "U1" H 2200 1250 60  0000 L CNN
F 1 "TL072" H 2200 800 60  0000 L CNN
F 2 "Housings_DIP:DIP-8_W7.62mm_LongPads" H 2250 1050 60  0001 C CNN
F 3 "" H 2250 1050 60  0000 C CNN
	1    2250 1050
	1    0    0    -1  
$EndComp
$Comp
L R R1
U 1 1 560AAEEF
P 1150 950
F 0 "R1" V 1230 950 50  0000 C CNN
F 1 "1k" V 1150 950 50  0000 C CNN
F 2 "Resistors_ThroughHole:Resistor_Horizontal_RM15mm" V 1080 950 30  0001 C CNN
F 3 "" H 1150 950 30  0000 C CNN
	1    1150 950 
	0    1    1    0   
$EndComp
$Comp
L R R2
U 1 1 560AAF35
P 1400 3200
F 0 "R2" V 1480 3200 50  0000 C CNN
F 1 "100k" V 1400 3200 50  0000 C CNN
F 2 "Resistors_ThroughHole:Resistor_Horizontal_RM15mm" V 1330 3200 30  0001 C CNN
F 3 "" H 1400 3200 30  0000 C CNN
	1    1400 3200
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR01
U 1 1 560AB094
P 1400 3550
F 0 "#PWR01" H 1400 3300 50  0001 C CNN
F 1 "GND" H 1400 3400 50  0000 C CNN
F 2 "" H 1400 3550 60  0000 C CNN
F 3 "" H 1400 3550 60  0000 C CNN
	1    1400 3550
	1    0    0    -1  
$EndComp
$Sheet
S 3750 1750 650  400 
U 560B176B
F0 "Woofers crossover" 60
F1 "wooferssch.sch" 60
F2 "LOW_IN" I L 3750 1850 60 
$EndSheet
$Comp
L TL072-RESCUE-subwoofer_LR U1
U 2 1 560C0E1E
P 4600 4800
F 0 "U1" H 4550 5000 60  0000 L CNN
F 1 "TL072" H 4550 4550 60  0000 L CNN
F 2 "Housings_DIP:DIP-8_W7.62mm_LongPads" H 4600 4800 60  0001 C CNN
F 3 "" H 4600 4800 60  0000 C CNN
	2    4600 4800
	1    0    0    -1  
$EndComp
$Comp
L R R3
U 1 1 560C126A
P 3000 4700
F 0 "R3" V 3080 4700 50  0000 C CNN
F 1 "10k" V 3000 4700 50  0000 C CNN
F 2 "Resistors_ThroughHole:Resistor_Horizontal_RM15mm" V 2930 4700 30  0001 C CNN
F 3 "" H 3000 4700 30  0000 C CNN
	1    3000 4700
	0    1    1    0   
$EndComp
$Comp
L R R4
U 1 1 560C134D
P 3450 4700
F 0 "R4" V 3530 4700 50  0000 C CNN
F 1 "10k" V 3450 4700 50  0000 C CNN
F 2 "Resistors_ThroughHole:Resistor_Horizontal_RM15mm" V 3380 4700 30  0001 C CNN
F 3 "" H 3450 4700 30  0000 C CNN
	1    3450 4700
	0    1    1    0   
$EndComp
$Comp
L C C1
U 1 1 560C13E7
P 3750 5000
F 0 "C1" H 3775 5100 50  0000 L CNN
F 1 "100n" H 3775 4900 50  0000 L CNN
F 2 "Capacitors_ThroughHole:C_Disc_D6_P5" H 3788 4850 30  0001 C CNN
F 3 "" H 3750 5000 60  0000 C CNN
	1    3750 5000
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR02
U 1 1 560C144E
P 3750 5250
F 0 "#PWR02" H 3750 5000 50  0001 C CNN
F 1 "GND" H 3750 5100 50  0000 C CNN
F 2 "" H 3750 5250 60  0000 C CNN
F 3 "" H 3750 5250 60  0000 C CNN
	1    3750 5250
	1    0    0    -1  
$EndComp
$Comp
L C C3
U 1 1 560C4FDB
P 4050 4150
F 0 "C3" H 4075 4250 50  0000 L CNN
F 1 "100n" H 4075 4050 50  0000 L CNN
F 2 "Capacitors_ThroughHole:C_Disc_D6_P5" H 4088 4000 30  0001 C CNN
F 3 "" H 4050 4150 60  0000 C CNN
	1    4050 4150
	0    1    1    0   
$EndComp
$Comp
L C C2
U 1 1 560C5074
P 4050 3900
F 0 "C2" H 4075 4000 50  0000 L CNN
F 1 "100n" H 4075 3800 50  0000 L CNN
F 2 "Capacitors_ThroughHole:C_Disc_D6_P5" H 4088 3750 30  0001 C CNN
F 3 "" H 4050 3900 60  0000 C CNN
	1    4050 3900
	0    1    1    0   
$EndComp
$Comp
L R R5
U 1 1 560C61ED
P 5350 4800
F 0 "R5" V 5430 4800 50  0000 C CNN
F 1 "10k" V 5350 4800 50  0000 C CNN
F 2 "Resistors_ThroughHole:Resistor_Horizontal_RM15mm" V 5280 4800 30  0001 C CNN
F 3 "" H 5350 4800 30  0000 C CNN
	1    5350 4800
	0    1    1    0   
$EndComp
$Comp
L R R6
U 1 1 560C61F3
P 5800 4800
F 0 "R6" V 5880 4800 50  0000 C CNN
F 1 "10k" V 5800 4800 50  0000 C CNN
F 2 "Resistors_ThroughHole:Resistor_Horizontal_RM15mm" V 5730 4800 30  0001 C CNN
F 3 "" H 5800 4800 30  0000 C CNN
	1    5800 4800
	0    1    1    0   
$EndComp
$Comp
L C C4
U 1 1 560C61F9
P 6100 5100
F 0 "C4" H 6125 5200 50  0000 L CNN
F 1 "100n" H 6125 5000 50  0000 L CNN
F 2 "Capacitors_ThroughHole:C_Disc_D6_P5" H 6138 4950 30  0001 C CNN
F 3 "" H 6100 5100 60  0000 C CNN
	1    6100 5100
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR03
U 1 1 560C61FF
P 6100 5350
F 0 "#PWR03" H 6100 5100 50  0001 C CNN
F 1 "GND" H 6100 5200 50  0000 C CNN
F 2 "" H 6100 5350 60  0000 C CNN
F 3 "" H 6100 5350 60  0000 C CNN
	1    6100 5350
	1    0    0    -1  
$EndComp
$Comp
L C C6
U 1 1 560C620F
P 6400 4250
F 0 "C6" H 6425 4350 50  0000 L CNN
F 1 "100n" H 6425 4150 50  0000 L CNN
F 2 "Capacitors_ThroughHole:C_Disc_D6_P5" H 6438 4100 30  0001 C CNN
F 3 "" H 6400 4250 60  0000 C CNN
	1    6400 4250
	0    1    1    0   
$EndComp
$Comp
L C C5
U 1 1 560C6215
P 6400 4000
F 0 "C5" H 6425 4100 50  0000 L CNN
F 1 "100n" H 6425 3900 50  0000 L CNN
F 2 "Capacitors_ThroughHole:C_Disc_D6_P5" H 6438 3850 30  0001 C CNN
F 3 "" H 6400 4000 60  0000 C CNN
	1    6400 4000
	0    1    1    0   
$EndComp
$Comp
L CONN_01X02 P3
U 1 1 560C6F29
P 9650 4850
F 0 "P3" H 9650 5000 50  0000 C CNN
F 1 "SUB_OUT" V 9750 4850 50  0000 C CNN
F 2 "Connect:bornier2" H 9650 4850 60  0001 C CNN
F 3 "" H 9650 4850 60  0000 C CNN
	1    9650 4850
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR04
U 1 1 560C7385
P 8150 5900
F 0 "#PWR04" H 8150 5650 50  0001 C CNN
F 1 "GND" H 8150 5750 50  0000 C CNN
F 2 "" H 8150 5900 60  0000 C CNN
F 3 "" H 8150 5900 60  0000 C CNN
	1    8150 5900
	1    0    0    -1  
$EndComp
$Comp
L CONN_01X03 P2
U 1 1 560C9D76
P 1200 6250
F 0 "P2" H 1200 6450 50  0000 C CNN
F 1 "SUPPLY" V 1300 6250 50  0000 C CNN
F 2 "Connect:bornier3" H 1200 6250 60  0001 C CNN
F 3 "" H 1200 6250 60  0000 C CNN
	1    1200 6250
	-1   0    0    1   
$EndComp
$Comp
L GND #PWR05
U 1 1 560CA56E
P 1600 6500
F 0 "#PWR05" H 1600 6250 50  0001 C CNN
F 1 "GND" H 1600 6350 50  0000 C CNN
F 2 "" H 1600 6500 60  0000 C CNN
F 3 "" H 1600 6500 60  0000 C CNN
	1    1600 6500
	1    0    0    -1  
$EndComp
Text GLabel 3550 6150 2    60   Input ~ 0
+15V
Text GLabel 3550 6250 2    60   Input ~ 0
-15V
Text GLabel 4500 4400 2    60   Input ~ 0
+15V
Text GLabel 4500 5200 2    60   Input ~ 0
-15V
Text GLabel 2150 650  2    60   Input ~ 0
+15V
$Sheet
S 3750 2400 700  400 
U 560E28AE
F0 "Tweeters crossover" 60
F1 "tweeters_crossover.sch" 60
F2 "HIGH_IN" I L 3750 2500 60 
$EndSheet
$Comp
L TL072-RESCUE-subwoofer_LR U2
U 1 1 560F37AA
P 6950 4900
F 0 "U2" H 6900 5100 60  0000 L CNN
F 1 "TL072" H 6900 4650 60  0000 L CNN
F 2 "Housings_DIP:DIP-8_W7.62mm_LongPads" H 6950 4900 60  0001 C CNN
F 3 "" H 6950 4900 60  0000 C CNN
	1    6950 4900
	1    0    0    -1  
$EndComp
Text GLabel 6850 4500 2    60   Input ~ 0
+15V
Text GLabel 6850 5300 2    60   Input ~ 0
-15V
$Comp
L TL072-RESCUE-subwoofer_LR U2
U 2 1 560CC5CF
P 8650 4800
F 0 "U2" H 8600 5000 60  0000 L CNN
F 1 "TL072" H 8600 4550 60  0000 L CNN
F 2 "Housings_DIP:DIP-8_W7.62mm_LongPads" H 8650 4800 60  0001 C CNN
F 3 "" H 8650 4800 60  0000 C CNN
	2    8650 4800
	1    0    0    -1  
$EndComp
$Comp
L R R10
U 1 1 560CD428
P 9300 4800
F 0 "R10" V 9380 4800 50  0000 C CNN
F 1 "100" V 9300 4800 50  0000 C CNN
F 2 "Resistors_ThroughHole:Resistor_Horizontal_RM15mm" V 9230 4800 30  0001 C CNN
F 3 "" H 9300 4800 30  0000 C CNN
	1    9300 4800
	0    1    1    0   
$EndComp
$Comp
L R R9
U 1 1 560CDD3A
P 8550 5450
F 0 "R9" V 8630 5450 50  0000 C CNN
F 1 "10k" V 8550 5450 50  0000 C CNN
F 2 "Resistors_ThroughHole:Resistor_Horizontal_RM15mm" V 8480 5450 30  0001 C CNN
F 3 "" H 8550 5450 30  0000 C CNN
	1    8550 5450
	0    1    1    0   
$EndComp
$Comp
L R R8
U 1 1 560CDDF1
P 8150 5650
F 0 "R8" V 8230 5650 50  0000 C CNN
F 1 "10k" V 8150 5650 50  0000 C CNN
F 2 "Resistors_ThroughHole:Resistor_Horizontal_RM15mm" V 8080 5650 30  0001 C CNN
F 3 "" H 8150 5650 30  0000 C CNN
	1    8150 5650
	1    0    0    -1  
$EndComp
$Comp
L R R7
U 1 1 560CDE8C
P 7950 5650
F 0 "R7" V 8030 5650 50  0000 C CNN
F 1 "10k" V 7950 5650 50  0000 C CNN
F 2 "Resistors_ThroughHole:Resistor_Horizontal_RM15mm" V 7880 5650 30  0001 C CNN
F 3 "" H 7950 5650 30  0000 C CNN
	1    7950 5650
	1    0    0    -1  
$EndComp
$Comp
L POT-RESCUE-subwoofer_LR RV1
U 1 1 560CDFE8
P 7750 5250
F 0 "RV1" H 7750 5150 50  0000 C CNN
F 1 "10k" H 7750 5250 50  0000 C CNN
F 2 "Potentiometers:Potentiometer_Trimmer-Piher-PT15-h5_vertical" H 7750 5250 60  0001 C CNN
F 3 "" H 7750 5250 60  0000 C CNN
	1    7750 5250
	0    1    1    0   
$EndComp
Text GLabel 8550 4400 2    60   Input ~ 0
+15V
Text GLabel 8550 5200 2    60   Input ~ 0
-15V
Wire Wire Line
	1000 950  900  950 
Wire Wire Line
	1300 950  1750 950 
Connection ~ 1400 950 
Wire Wire Line
	1750 1150 1700 1150
Wire Wire Line
	1700 1150 1700 1600
Wire Wire Line
	1700 1600 2750 1600
Wire Wire Line
	3750 5250 3750 5150
Wire Wire Line
	3750 4850 3750 4700
Wire Wire Line
	3600 4700 4100 4700
Connection ~ 3750 4700
Wire Wire Line
	3300 4700 3150 4700
Wire Wire Line
	2750 4700 2850 4700
Wire Wire Line
	4100 4900 4000 4900
Wire Wire Line
	4000 4900 4000 5400
Wire Wire Line
	4000 5400 5100 5400
Wire Wire Line
	5100 5400 5100 3900
Wire Wire Line
	3250 4700 3250 3900
Connection ~ 3250 4700
Wire Wire Line
	5100 3900 4200 3900
Connection ~ 5100 4800
Wire Wire Line
	3900 4150 3800 4150
Wire Wire Line
	3800 4150 3800 3900
Connection ~ 3800 3900
Wire Wire Line
	4200 4150 4350 4150
Wire Wire Line
	4350 4150 4350 3900
Connection ~ 4350 3900
Wire Wire Line
	6100 5350 6100 5250
Wire Wire Line
	6100 4950 6100 4800
Wire Wire Line
	5950 4800 6450 4800
Connection ~ 6100 4800
Wire Wire Line
	5650 4800 5500 4800
Wire Wire Line
	5100 4800 5200 4800
Wire Wire Line
	6450 5000 6350 5000
Wire Wire Line
	6350 5000 6350 5500
Wire Wire Line
	6350 5500 7450 5500
Wire Wire Line
	7450 5500 7450 4000
Wire Wire Line
	5600 4800 5600 4000
Wire Wire Line
	5600 4000 6250 4000
Connection ~ 5600 4800
Wire Wire Line
	7450 4000 6550 4000
Connection ~ 7450 4900
Wire Wire Line
	6250 4250 6150 4250
Wire Wire Line
	6150 4250 6150 4000
Connection ~ 6150 4000
Wire Wire Line
	6550 4250 6700 4250
Wire Wire Line
	6700 4250 6700 4000
Connection ~ 6700 4000
Wire Wire Line
	1400 6350 1800 6350
Wire Wire Line
	1600 6350 1600 6500
Wire Wire Line
	3400 1050 3400 2500
Wire Wire Line
	3400 1850 3750 1850
Wire Wire Line
	7750 5500 7750 5800
Wire Wire Line
	7750 5800 9450 5800
Connection ~ 7950 5800
Wire Wire Line
	8150 5800 8150 5900
Connection ~ 8150 5800
Wire Wire Line
	7900 5250 7950 5250
Wire Wire Line
	7950 4700 7950 5500
Wire Wire Line
	7950 4700 8150 4700
Connection ~ 7950 5250
Wire Wire Line
	8150 5500 8150 4900
Wire Wire Line
	8400 5450 8150 5450
Connection ~ 8150 5450
Wire Wire Line
	9150 5450 8700 5450
Wire Wire Line
	9150 4800 9150 5450
Wire Wire Line
	9450 5800 9450 4900
Wire Wire Line
	7750 5000 7750 4900
Wire Wire Line
	7750 4900 7450 4900
Text GLabel 2150 1450 2    60   Input ~ 0
-15V
Wire Wire Line
	3400 1050 2750 1050
Wire Wire Line
	3400 2500 3750 2500
Connection ~ 3400 1850
Wire Wire Line
	1400 950  1400 3050
Wire Wire Line
	3250 3900 3900 3900
Wire Wire Line
	2750 1050 2750 4700
Wire Wire Line
	1400 3550 1400 3350
$Comp
L GND #PWR06
U 1 1 561AA7F5
P 900 1300
F 0 "#PWR06" H 900 1050 50  0001 C CNN
F 1 "GND" H 900 1150 50  0000 C CNN
F 2 "" H 900 1300 60  0000 C CNN
F 3 "" H 900 1300 60  0000 C CNN
	1    900  1300
	1    0    0    -1  
$EndComp
Wire Wire Line
	900  1300 900  1050
Connection ~ 2750 1600
Wire Wire Line
	1400 6150 3550 6150
Wire Wire Line
	3550 6250 1400 6250
$Comp
L C C14
U 1 1 563EA44D
P 2050 6400
F 0 "C14" H 2075 6500 50  0000 L CNN
F 1 "C" H 2075 6300 50  0000 L CNN
F 2 "Capacitors_ThroughHole:C_Disc_D3_P2.5" H 2088 6250 30  0001 C CNN
F 3 "" H 2050 6400 60  0000 C CNN
	1    2050 6400
	1    0    0    -1  
$EndComp
$Comp
L C C16
U 1 1 563EA4BA
P 2350 6400
F 0 "C16" H 2375 6500 50  0000 L CNN
F 1 "C" H 2375 6300 50  0000 L CNN
F 2 "Capacitors_ThroughHole:C_Disc_D3_P2.5" H 2388 6250 30  0001 C CNN
F 3 "" H 2350 6400 60  0000 C CNN
	1    2350 6400
	1    0    0    -1  
$EndComp
$Comp
L C C22
U 1 1 563EA515
P 2650 6400
F 0 "C22" H 2675 6500 50  0000 L CNN
F 1 "C" H 2675 6300 50  0000 L CNN
F 2 "Capacitors_ThroughHole:C_Disc_D3_P2.5" H 2688 6250 30  0001 C CNN
F 3 "" H 2650 6400 60  0000 C CNN
	1    2650 6400
	1    0    0    -1  
$EndComp
$Comp
L C C13
U 1 1 563EA595
P 2050 6000
F 0 "C13" H 2075 6100 50  0000 L CNN
F 1 "C" H 2075 5900 50  0000 L CNN
F 2 "Capacitors_ThroughHole:C_Disc_D3_P2.5" H 2088 5850 30  0001 C CNN
F 3 "" H 2050 6000 60  0000 C CNN
	1    2050 6000
	1    0    0    -1  
$EndComp
$Comp
L C C15
U 1 1 563EA615
P 2350 6000
F 0 "C15" H 2375 6100 50  0000 L CNN
F 1 "C" H 2375 5900 50  0000 L CNN
F 2 "Capacitors_ThroughHole:C_Disc_D3_P2.5" H 2388 5850 30  0001 C CNN
F 3 "" H 2350 6000 60  0000 C CNN
	1    2350 6000
	1    0    0    -1  
$EndComp
$Comp
L C C21
U 1 1 563EA694
P 2650 6000
F 0 "C21" H 2675 6100 50  0000 L CNN
F 1 "C" H 2675 5900 50  0000 L CNN
F 2 "Capacitors_ThroughHole:C_Disc_D3_P2.5" H 2688 5850 30  0001 C CNN
F 3 "" H 2650 6000 60  0000 C CNN
	1    2650 6000
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR07
U 1 1 563EA718
P 1700 5550
F 0 "#PWR07" H 1700 5300 50  0001 C CNN
F 1 "GND" H 1700 5400 50  0000 C CNN
F 2 "" H 1700 5550 60  0000 C CNN
F 3 "" H 1700 5550 60  0000 C CNN
	1    1700 5550
	1    0    0    -1  
$EndComp
Wire Wire Line
	1700 5550 2050 5550
Wire Wire Line
	2050 5550 2050 5850
Wire Wire Line
	2050 5850 2650 5850
Connection ~ 2350 5850
Wire Wire Line
	1800 6550 2650 6550
Connection ~ 2350 6550
Wire Wire Line
	1800 6350 1800 6550
Connection ~ 1600 6350
Connection ~ 2050 6550
Connection ~ 2050 5850
Connection ~ 2050 6150
Connection ~ 2350 6150
Connection ~ 2650 6150
Connection ~ 2650 6250
Connection ~ 2350 6250
Connection ~ 2050 6250
$EndSCHEMATC

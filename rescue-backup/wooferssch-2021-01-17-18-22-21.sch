EESchema Schematic File Version 2
LIBS:subwoofer_LR-rescue
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:subwoofer_LR-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 2 3
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L TL072-RESCUE-subwoofer_LR U3
U 1 1 560D55B7
P 2850 1650
F 0 "U3" H 2800 1850 60  0000 L CNN
F 1 "TL072" H 2800 1400 60  0000 L CNN
F 2 "Housings_DIP:DIP-8_W7.62mm_LongPads" H 2850 1650 60  0001 C CNN
F 3 "" H 2850 1650 60  0000 C CNN
	1    2850 1650
	1    0    0    -1  
$EndComp
$Comp
L R R11
U 1 1 560D55BE
P 1250 1550
F 0 "R11" V 1330 1550 50  0000 C CNN
F 1 "4.7k" V 1250 1550 50  0000 C CNN
F 2 "Resistors_ThroughHole:Resistor_Horizontal_RM15mm" V 1180 1550 30  0001 C CNN
F 3 "" H 1250 1550 30  0000 C CNN
	1    1250 1550
	0    1    1    0   
$EndComp
$Comp
L R R12
U 1 1 560D55C5
P 1700 1550
F 0 "R12" V 1780 1550 50  0000 C CNN
F 1 "4.7k" V 1700 1550 50  0000 C CNN
F 2 "Resistors_ThroughHole:Resistor_Horizontal_RM15mm" V 1630 1550 30  0001 C CNN
F 3 "" H 1700 1550 30  0000 C CNN
	1    1700 1550
	0    1    1    0   
$EndComp
$Comp
L C C7
U 1 1 560D55CC
P 2000 1850
F 0 "C7" H 2025 1950 50  0000 L CNN
F 1 "10n" H 2025 1750 50  0000 L CNN
F 2 "Capacitors_ThroughHole:C_Disc_D6_P5" H 2038 1700 30  0001 C CNN
F 3 "" H 2000 1850 60  0000 C CNN
	1    2000 1850
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR08
U 1 1 560D55D3
P 2000 2100
F 0 "#PWR08" H 2000 1850 50  0001 C CNN
F 1 "GND" H 2000 1950 50  0000 C CNN
F 2 "" H 2000 2100 60  0000 C CNN
F 3 "" H 2000 2100 60  0000 C CNN
	1    2000 2100
	1    0    0    -1  
$EndComp
Wire Wire Line
	2000 2100 2000 2000
Wire Wire Line
	2000 1700 2000 1550
Wire Wire Line
	1850 1550 2350 1550
Connection ~ 2000 1550
Wire Wire Line
	1550 1550 1400 1550
Wire Wire Line
	1000 1550 1100 1550
Wire Wire Line
	2350 1750 2250 1750
Wire Wire Line
	2250 1750 2250 2250
Wire Wire Line
	2250 2250 3350 2250
Wire Wire Line
	3350 2250 3350 750 
$Comp
L C C9
U 1 1 560D55E3
P 2300 1000
F 0 "C9" H 2325 1100 50  0000 L CNN
F 1 "10n" H 2325 900 50  0000 L CNN
F 2 "Capacitors_ThroughHole:C_Disc_D6_P5" H 2338 850 30  0001 C CNN
F 3 "" H 2300 1000 60  0000 C CNN
	1    2300 1000
	0    1    1    0   
$EndComp
$Comp
L C C8
U 1 1 560D55EA
P 2300 750
F 0 "C8" H 2325 850 50  0000 L CNN
F 1 "10n" H 2325 650 50  0000 L CNN
F 2 "Capacitors_ThroughHole:C_Disc_D6_P5" H 2338 600 30  0001 C CNN
F 3 "" H 2300 750 60  0000 C CNN
	1    2300 750 
	0    1    1    0   
$EndComp
Wire Wire Line
	1500 1550 1500 750 
Wire Wire Line
	1500 750  2150 750 
Connection ~ 1500 1550
Wire Wire Line
	3350 750  2450 750 
Connection ~ 3350 1650
Wire Wire Line
	2150 1000 2050 1000
Wire Wire Line
	2050 1000 2050 750 
Connection ~ 2050 750 
Wire Wire Line
	2450 1000 2600 1000
Wire Wire Line
	2600 1000 2600 750 
Connection ~ 2600 750 
Wire Wire Line
	3350 1650 3550 1650
Text GLabel 2750 1250 2    60   Input ~ 0
+15V
Text GLabel 2750 2050 2    60   Input ~ 0
-15V
$Comp
L TL072-RESCUE-subwoofer_LR U4
U 1 1 560D5A8E
P 5300 1750
F 0 "U4" H 5250 1950 60  0000 L CNN
F 1 "TL072" H 5250 1500 60  0000 L CNN
F 2 "Housings_DIP:DIP-8_W7.62mm_LongPads" H 5300 1750 60  0001 C CNN
F 3 "" H 5300 1750 60  0000 C CNN
	1    5300 1750
	1    0    0    -1  
$EndComp
$Comp
L R R13
U 1 1 560D5A94
P 3700 1650
F 0 "R13" V 3780 1650 50  0000 C CNN
F 1 "4.7k" V 3700 1650 50  0000 C CNN
F 2 "Resistors_ThroughHole:Resistor_Horizontal_RM15mm" V 3630 1650 30  0001 C CNN
F 3 "" H 3700 1650 30  0000 C CNN
	1    3700 1650
	0    1    1    0   
$EndComp
$Comp
L R R14
U 1 1 560D5A9A
P 4150 1650
F 0 "R14" V 4230 1650 50  0000 C CNN
F 1 "4.7k" V 4150 1650 50  0000 C CNN
F 2 "Resistors_ThroughHole:Resistor_Horizontal_RM15mm" V 4080 1650 30  0001 C CNN
F 3 "" H 4150 1650 30  0000 C CNN
	1    4150 1650
	0    1    1    0   
$EndComp
$Comp
L C C10
U 1 1 560D5AA0
P 4450 1950
F 0 "C10" H 4475 2050 50  0000 L CNN
F 1 "10n" H 4475 1850 50  0000 L CNN
F 2 "Capacitors_ThroughHole:C_Disc_D6_P5" H 4488 1800 30  0001 C CNN
F 3 "" H 4450 1950 60  0000 C CNN
	1    4450 1950
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR09
U 1 1 560D5AA6
P 4450 2200
F 0 "#PWR09" H 4450 1950 50  0001 C CNN
F 1 "GND" H 4450 2050 50  0000 C CNN
F 2 "" H 4450 2200 60  0000 C CNN
F 3 "" H 4450 2200 60  0000 C CNN
	1    4450 2200
	1    0    0    -1  
$EndComp
Wire Wire Line
	4450 2200 4450 2100
Wire Wire Line
	4450 1800 4450 1650
Wire Wire Line
	4300 1650 4800 1650
Connection ~ 4450 1650
Wire Wire Line
	4000 1650 3850 1650
Wire Wire Line
	4800 1850 4700 1850
Wire Wire Line
	4700 1850 4700 2350
Wire Wire Line
	4700 2350 5800 2350
Wire Wire Line
	5800 2350 5800 850 
$Comp
L C C12
U 1 1 560D5AB6
P 4750 1100
F 0 "C12" H 4775 1200 50  0000 L CNN
F 1 "10n" H 4775 1000 50  0000 L CNN
F 2 "Capacitors_ThroughHole:C_Disc_D6_P5" H 4788 950 30  0001 C CNN
F 3 "" H 4750 1100 60  0000 C CNN
	1    4750 1100
	0    1    1    0   
$EndComp
$Comp
L C C11
U 1 1 560F931D
P 4750 850
F 0 "C11" H 4775 950 50  0000 L CNN
F 1 "10n" H 4775 750 50  0000 L CNN
F 2 "Capacitors_ThroughHole:C_Disc_D6_P5" H 4788 700 30  0001 C CNN
F 3 "" H 4750 850 60  0000 C CNN
	1    4750 850 
	0    1    1    0   
$EndComp
Wire Wire Line
	3950 1650 3950 850 
Wire Wire Line
	3950 850  4600 850 
Connection ~ 3950 1650
Wire Wire Line
	5800 850  4900 850 
Connection ~ 5800 1750
Wire Wire Line
	4600 1100 4500 1100
Wire Wire Line
	4500 1100 4500 850 
Connection ~ 4500 850 
Wire Wire Line
	4900 1100 5050 1100
Wire Wire Line
	5050 1100 5050 850 
Connection ~ 5050 850 
Wire Wire Line
	5800 1750 6200 1750
Text GLabel 5200 1350 2    60   Input ~ 0
+15V
Text GLabel 5200 2150 2    60   Input ~ 0
-15V
Text HLabel 1000 1550 0    60   Input ~ 0
LOW_IN
$Comp
L CONN_01X02 P4
U 1 1 560DF131
P 8100 1600
F 0 "P4" H 8100 1750 50  0000 C CNN
F 1 "LOW_OUT" V 8200 1600 50  0000 C CNN
F 2 "Connect:bornier2" H 8100 1600 60  0001 C CNN
F 3 "" H 8100 1600 60  0000 C CNN
	1    8100 1600
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR010
U 1 1 560D5ABC
P 6600 2650
F 0 "#PWR010" H 6600 2400 50  0001 C CNN
F 1 "GND" H 6600 2500 50  0000 C CNN
F 2 "" H 6600 2650 60  0000 C CNN
F 3 "" H 6600 2650 60  0000 C CNN
	1    6600 2650
	1    0    0    -1  
$EndComp
$Comp
L TL072-RESCUE-subwoofer_LR U5
U 1 1 560D5AC2
P 7100 1550
F 0 "U5" H 7050 1750 60  0000 L CNN
F 1 "TL072" H 7050 1300 60  0000 L CNN
F 2 "Housings_DIP:DIP-8_W7.62mm_LongPads" H 7100 1550 60  0001 C CNN
F 3 "" H 7100 1550 60  0000 C CNN
	1    7100 1550
	1    0    0    -1  
$EndComp
$Comp
L R R22
U 1 1 560D5AC9
P 7750 1550
F 0 "R22" V 7830 1550 50  0000 C CNN
F 1 "100" V 7750 1550 50  0000 C CNN
F 2 "Resistors_ThroughHole:Resistor_Horizontal_RM15mm" V 7680 1550 30  0001 C CNN
F 3 "" H 7750 1550 30  0000 C CNN
	1    7750 1550
	0    1    1    0   
$EndComp
$Comp
L R R21
U 1 1 560D5AD0
P 7000 2200
F 0 "R21" V 7080 2200 50  0000 C CNN
F 1 "10k" V 7000 2200 50  0000 C CNN
F 2 "Resistors_ThroughHole:Resistor_Horizontal_RM15mm" V 6930 2200 30  0001 C CNN
F 3 "" H 7000 2200 30  0000 C CNN
	1    7000 2200
	0    1    1    0   
$EndComp
$Comp
L R R18
U 1 1 560D5AD7
P 6600 2400
F 0 "R18" V 6680 2400 50  0000 C CNN
F 1 "10k" V 6600 2400 50  0000 C CNN
F 2 "Resistors_ThroughHole:Resistor_Horizontal_RM15mm" V 6530 2400 30  0001 C CNN
F 3 "" H 6600 2400 30  0000 C CNN
	1    6600 2400
	1    0    0    -1  
$EndComp
$Comp
L R R17
U 1 1 560D5ADE
P 6400 2400
F 0 "R17" V 6480 2400 50  0000 C CNN
F 1 "10k" V 6400 2400 50  0000 C CNN
F 2 "Resistors_ThroughHole:Resistor_Horizontal_RM15mm" V 6330 2400 30  0001 C CNN
F 3 "" H 6400 2400 30  0000 C CNN
	1    6400 2400
	1    0    0    -1  
$EndComp
$Comp
L POT-RESCUE-subwoofer_LR RV2
U 1 1 560D5AE5
P 6200 2000
AR Path="/560D5AE5" Ref="RV2"  Part="1" 
AR Path="/560B176B/560D5AE5" Ref="RV2"  Part="1" 
F 0 "RV2" H 6200 1900 50  0000 C CNN
F 1 "10k" H 6200 2000 50  0000 C CNN
F 2 "Potentiometers:Potentiometer_WirePads_largePads" H 6200 2000 60  0001 C CNN
F 3 "" H 6200 2000 60  0000 C CNN
	1    6200 2000
	0    1    1    0   
$EndComp
Wire Wire Line
	6200 2250 6200 2550
Wire Wire Line
	6200 2550 7900 2550
Connection ~ 6400 2550
Wire Wire Line
	6600 2550 6600 2650
Connection ~ 6600 2550
Wire Wire Line
	6350 2000 6400 2000
Wire Wire Line
	6400 1450 6400 2250
Wire Wire Line
	6400 1450 6600 1450
Connection ~ 6400 2000
Wire Wire Line
	6600 2250 6600 1650
Wire Wire Line
	6850 2200 6600 2200
Connection ~ 6600 2200
Wire Wire Line
	7600 2200 7150 2200
Wire Wire Line
	7600 1550 7600 2200
Wire Wire Line
	7900 2550 7900 1650
Text GLabel 7000 1150 2    60   Input ~ 0
+15V
Text GLabel 7000 1950 2    60   Input ~ 0
-15V
$EndSCHEMATC

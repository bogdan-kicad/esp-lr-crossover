EESchema Schematic File Version 2
LIBS:subwoofer_LR-rescue
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:subwoofer_LR-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 3 3
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L C C17
U 1 1 560E3D99
P 2100 1350
F 0 "C17" H 2125 1450 50  0000 L CNN
F 1 "10n" H 2125 1250 50  0000 L CNN
F 2 "Capacitors_ThroughHole:C_Disc_D6_P5" H 2138 1200 30  0001 C CNN
F 3 "" H 2100 1350 60  0000 C CNN
	1    2100 1350
	0    1    1    0   
$EndComp
$Comp
L C C18
U 1 1 560E3DA0
P 2500 1350
F 0 "C18" H 2525 1450 50  0000 L CNN
F 1 "10n" H 2525 1250 50  0000 L CNN
F 2 "Capacitors_ThroughHole:C_Disc_D6_P5" H 2538 1200 30  0001 C CNN
F 3 "" H 2500 1350 60  0000 C CNN
	1    2500 1350
	0    1    1    0   
$EndComp
$Comp
L R R24
U 1 1 560E3DA7
P 2850 1600
F 0 "R24" V 2930 1600 50  0000 C CNN
F 1 "4.7k" V 2850 1600 50  0000 C CNN
F 2 "Resistors_ThroughHole:Resistor_Horizontal_RM15mm" V 2780 1600 30  0001 C CNN
F 3 "" H 2850 1600 30  0000 C CNN
	1    2850 1600
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR011
U 1 1 560E3DAE
P 2850 2150
F 0 "#PWR011" H 2850 1900 50  0001 C CNN
F 1 "GND" H 2850 2000 50  0000 C CNN
F 2 "" H 2850 2150 60  0000 C CNN
F 3 "" H 2850 2150 60  0000 C CNN
	1    2850 2150
	1    0    0    -1  
$EndComp
Wire Wire Line
	2850 1450 2850 1350
Wire Wire Line
	2650 1350 3200 1350
Wire Wire Line
	2250 1350 2350 1350
$Comp
L TL072-RESCUE-subwoofer_LR U3
U 2 1 560E3DB8
P 3700 1450
F 0 "U3" H 3650 1650 60  0000 L CNN
F 1 "TL072" H 3650 1200 60  0000 L CNN
F 2 "Housings_DIP:DIP-8_W7.62mm_LongPads" H 3700 1450 60  0001 C CNN
F 3 "" H 3700 1450 60  0000 C CNN
	2    3700 1450
	1    0    0    -1  
$EndComp
$Comp
L TL072-RESCUE-subwoofer_LR U4
U 2 1 560E3DBF
P 6050 1550
F 0 "U4" H 6000 1750 60  0000 L CNN
F 1 "TL072" H 6000 1300 60  0000 L CNN
F 2 "Housings_DIP:DIP-8_W7.62mm_LongPads" H 6050 1550 60  0001 C CNN
F 3 "" H 6050 1550 60  0000 C CNN
	2    6050 1550
	1    0    0    -1  
$EndComp
Connection ~ 2850 1350
Wire Wire Line
	3200 1550 3100 1550
Wire Wire Line
	3100 1550 3100 2000
Wire Wire Line
	3100 2000 4200 2000
Wire Wire Line
	4200 2000 4200 900 
$Comp
L R R23
U 1 1 560E3DCB
P 2850 900
F 0 "R23" V 2930 900 50  0000 C CNN
F 1 "4.7k" V 2850 900 50  0000 C CNN
F 2 "Resistors_ThroughHole:Resistor_Horizontal_RM15mm" V 2780 900 30  0001 C CNN
F 3 "" H 2850 900 30  0000 C CNN
	1    2850 900 
	0    1    1    0   
$EndComp
Wire Wire Line
	2300 1350 2300 900 
Wire Wire Line
	2300 900  2700 900 
Connection ~ 2300 1350
Wire Wire Line
	4200 900  3000 900 
Connection ~ 4200 1450
$Comp
L C C19
U 1 1 560E3DD7
P 4450 1450
F 0 "C19" H 4475 1550 50  0000 L CNN
F 1 "10n" H 4475 1350 50  0000 L CNN
F 2 "Capacitors_ThroughHole:C_Disc_D6_P5" H 4488 1300 30  0001 C CNN
F 3 "" H 4450 1450 60  0000 C CNN
	1    4450 1450
	0    1    1    0   
$EndComp
$Comp
L C C20
U 1 1 560E3DDE
P 4850 1450
F 0 "C20" H 4875 1550 50  0000 L CNN
F 1 "10n" H 4875 1350 50  0000 L CNN
F 2 "Capacitors_ThroughHole:C_Disc_D6_P5" H 4888 1300 30  0001 C CNN
F 3 "" H 4850 1450 60  0000 C CNN
	1    4850 1450
	0    1    1    0   
$EndComp
$Comp
L R R27
U 1 1 560E3DE5
P 5250 1650
F 0 "R27" V 5330 1650 50  0000 C CNN
F 1 "4.7k" V 5250 1650 50  0000 C CNN
F 2 "Resistors_ThroughHole:Resistor_Horizontal_RM15mm" V 5180 1650 30  0001 C CNN
F 3 "" H 5250 1650 30  0000 C CNN
	1    5250 1650
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR012
U 1 1 560E3DEC
P 5250 2200
F 0 "#PWR012" H 5250 1950 50  0001 C CNN
F 1 "GND" H 5250 2050 50  0000 C CNN
F 2 "" H 5250 2200 60  0000 C CNN
F 3 "" H 5250 2200 60  0000 C CNN
	1    5250 2200
	1    0    0    -1  
$EndComp
Wire Wire Line
	4300 1450 4200 1450
Wire Wire Line
	4600 1450 4700 1450
Wire Wire Line
	5000 1450 5550 1450
Wire Wire Line
	5250 1450 5250 1500
$Comp
L R R26
U 1 1 560E3DF7
P 5250 900
F 0 "R26" V 5330 900 50  0000 C CNN
F 1 "4.7k" V 5250 900 50  0000 C CNN
F 2 "Resistors_ThroughHole:Resistor_Horizontal_RM15mm" V 5180 900 30  0001 C CNN
F 3 "" H 5250 900 30  0000 C CNN
	1    5250 900 
	0    1    1    0   
$EndComp
Wire Wire Line
	4650 1450 4650 900 
Wire Wire Line
	4650 900  5100 900 
Connection ~ 4650 1450
Connection ~ 5250 1450
Wire Wire Line
	5550 1650 5450 1650
Wire Wire Line
	5450 1650 5450 2100
Wire Wire Line
	5450 2100 6550 2100
Wire Wire Line
	6550 2100 6550 900 
Wire Wire Line
	6550 900  5400 900 
Connection ~ 6550 1550
Text GLabel 3600 1050 2    60   Input ~ 0
+15V
Text GLabel 3600 1850 2    60   Input ~ 0
-15V
Text HLabel 1400 1350 0    60   Input ~ 0
HIGH_IN
Wire Wire Line
	1950 1350 1400 1350
$Comp
L R R25
U 1 1 560E7975
P 2850 1900
F 0 "R25" V 2930 1900 50  0000 C CNN
F 1 "4.7k" V 2850 1900 50  0000 C CNN
F 2 "Resistors_ThroughHole:Resistor_Horizontal_RM15mm" V 2780 1900 30  0001 C CNN
F 3 "" H 2850 1900 30  0000 C CNN
	1    2850 1900
	1    0    0    -1  
$EndComp
Wire Wire Line
	2850 2150 2850 2050
$Comp
L R R28
U 1 1 560E7F3E
P 5250 1950
F 0 "R28" V 5330 1950 50  0000 C CNN
F 1 "4.7k" V 5250 1950 50  0000 C CNN
F 2 "Resistors_ThroughHole:Resistor_Horizontal_RM15mm" V 5180 1950 30  0001 C CNN
F 3 "" H 5250 1950 30  0000 C CNN
	1    5250 1950
	1    0    0    -1  
$EndComp
Wire Wire Line
	5250 2200 5250 2100
$Comp
L CONN_01X02 P5
U 1 1 560E8A3D
P 9450 1500
F 0 "P5" H 9450 1650 50  0000 C CNN
F 1 "HIGH_OUT" V 9550 1500 50  0000 C CNN
F 2 "Connect:bornier2" H 9450 1500 60  0001 C CNN
F 3 "" H 9450 1500 60  0000 C CNN
	1    9450 1500
	1    0    0    -1  
$EndComp
Text GLabel 5950 1150 2    60   Input ~ 0
+15V
Text GLabel 5950 1950 2    60   Input ~ 0
-15V
$Comp
L GND #PWR013
U 1 1 560E6B40
P 7950 2550
F 0 "#PWR013" H 7950 2300 50  0001 C CNN
F 1 "GND" H 7950 2400 50  0000 C CNN
F 2 "" H 7950 2550 60  0000 C CNN
F 3 "" H 7950 2550 60  0000 C CNN
	1    7950 2550
	1    0    0    -1  
$EndComp
$Comp
L TL072-RESCUE-subwoofer_LR U5
U 2 1 560E6B46
P 8450 1450
F 0 "U5" H 8400 1650 60  0000 L CNN
F 1 "TL072" H 8400 1200 60  0000 L CNN
F 2 "Housings_DIP:DIP-8_W7.62mm_LongPads" H 8450 1450 60  0001 C CNN
F 3 "" H 8450 1450 60  0000 C CNN
	2    8450 1450
	1    0    0    -1  
$EndComp
$Comp
L R R32
U 1 1 560E6B4D
P 9100 1450
F 0 "R32" V 9180 1450 50  0000 C CNN
F 1 "100" V 9100 1450 50  0000 C CNN
F 2 "Resistors_ThroughHole:Resistor_Horizontal_RM15mm" V 9030 1450 30  0001 C CNN
F 3 "" H 9100 1450 30  0000 C CNN
	1    9100 1450
	0    1    1    0   
$EndComp
$Comp
L R R31
U 1 1 560E6B54
P 8350 2100
F 0 "R31" V 8430 2100 50  0000 C CNN
F 1 "10k" V 8350 2100 50  0000 C CNN
F 2 "Resistors_ThroughHole:Resistor_Horizontal_RM15mm" V 8280 2100 30  0001 C CNN
F 3 "" H 8350 2100 30  0000 C CNN
	1    8350 2100
	0    1    1    0   
$EndComp
$Comp
L R R30
U 1 1 560E6B5B
P 7950 2300
F 0 "R30" V 8030 2300 50  0000 C CNN
F 1 "10k" V 7950 2300 50  0000 C CNN
F 2 "Resistors_ThroughHole:Resistor_Horizontal_RM15mm" V 7880 2300 30  0001 C CNN
F 3 "" H 7950 2300 30  0000 C CNN
	1    7950 2300
	1    0    0    -1  
$EndComp
$Comp
L R R29
U 1 1 560E6B62
P 7750 2300
F 0 "R29" V 7830 2300 50  0000 C CNN
F 1 "10k" V 7750 2300 50  0000 C CNN
F 2 "Resistors_ThroughHole:Resistor_Horizontal_RM15mm" V 7680 2300 30  0001 C CNN
F 3 "" H 7750 2300 30  0000 C CNN
	1    7750 2300
	1    0    0    -1  
$EndComp
$Comp
L POT-RESCUE-subwoofer_LR RV3
U 1 1 560E6B69
P 7550 1900
F 0 "RV3" H 7550 1800 50  0000 C CNN
F 1 "10k" H 7550 1900 50  0000 C CNN
F 2 "Potentiometers:Potentiometer_WirePads_largePads" H 7550 1900 60  0001 C CNN
F 3 "" H 7550 1900 60  0000 C CNN
	1    7550 1900
	0    1    1    0   
$EndComp
Wire Wire Line
	7550 2150 7550 2450
Wire Wire Line
	7550 2450 9250 2450
Connection ~ 7750 2450
Wire Wire Line
	7950 2450 7950 2550
Connection ~ 7950 2450
Wire Wire Line
	7700 1900 7750 1900
Wire Wire Line
	7750 1350 7750 2150
Wire Wire Line
	7750 1350 7950 1350
Connection ~ 7750 1900
Wire Wire Line
	7950 2150 7950 1550
Wire Wire Line
	8200 2100 7950 2100
Connection ~ 7950 2100
Wire Wire Line
	8950 2100 8500 2100
Wire Wire Line
	8950 1450 8950 2100
Wire Wire Line
	9250 2450 9250 1550
Text GLabel 8350 1050 2    60   Input ~ 0
+15V
Text GLabel 8350 1850 2    60   Input ~ 0
-15V
Wire Wire Line
	7550 1550 7550 1650
Wire Wire Line
	6550 1550 7550 1550
$EndSCHEMATC
